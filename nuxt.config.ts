import svgLoader from "vite-svg-loader";
export default defineNuxtConfig({
  app: {
    head: {
      title: "HahuJobs",
      htmlAttrs: {
        lang: "en",
      },
      link: [
        {
          "data-n-head": "ssr",
          rel: "icon",
          type: "image/x-icon",
          href: "/images/favicon.ico",
        },
        {
          "data-n-head": "ssr",
          rel: "icon",
          type: "image/png",
          sizes: "16x16",
          href: "/images/favicon.ico",
        },
        {
          "data-n-head": "ssr",
          rel: "icon",
          type: "image/png",
          sizes: "32x32",
          href: "/images/favicon.ico",
        },
        {
          "data-n-head": "ssr",
          rel: "icon",
          type: "image/png",
          sizes: "96x96",
          href: "/images/favicon.ico",
        },
      ],
    },
  },
  css: ["~/assets/css/main.css"],
  modules: ["@nuxtjs/color-mode", "nuxt-headlessui"],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  colorMode: {
    preference: "system", // default value of $colorMode.preference
    fallback: "light", // fallback value if not system preference found
    hid: "nuxt-color-mode-script",
    globalName: "__NUXT_COLOR_MODE__",
    componentName: "ColorScheme",
    classPrefix: "",
    classSuffix: "",
    storageKey: "nuxt-color-mode",
  },
  build: {
    transpile: ["gsap", "@heroicons/vue"],
  },
  vite: {
    plugins: [svgLoader()],
  },
});
// "@vueuse/motion": "^2.0.0-beta.12",
